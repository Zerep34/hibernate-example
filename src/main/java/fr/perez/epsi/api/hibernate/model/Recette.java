package fr.perez.epsi.api.hibernate.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Recette {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "label")
	private String label;
	
	@Column(name = "description_rec")
	private String description_rec;
	
	@ManyToMany
	@JoinTable(name = "recettehasingredient",
		joinColumns = { @JoinColumn(name = "id_rec") },
	    inverseJoinColumns = { @JoinColumn(name = "id_ing") })
	private List<Ingredient> ingredients;

	public Recette() {}
	
	public Recette(String name, String description, List<Ingredient> ingredients) {
		super();
		this.label = name;
		this.description_rec = description;
		this.ingredients = ingredients;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return label;
	}

	public void setName(String name) {
		this.label = name;
	}

	public String getDescription() {
		return description_rec;
	}

	public void setDescription(String description) {
		this.description_rec = description;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "Recette{" +
				"id=" + id +
				", name='" + label + '\'' +
				", description='" + description_rec + '\'' +
				", ingredients=" + ingredients +
				'}';
	}
}
