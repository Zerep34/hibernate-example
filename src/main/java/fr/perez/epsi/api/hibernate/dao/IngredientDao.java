package fr.perez.epsi.api.hibernate.dao;

import fr.perez.epsi.api.hibernate.dao.helper.DatabaseHelper;
import fr.perez.epsi.api.hibernate.exception.AlreadyExistsException;
import fr.perez.epsi.api.hibernate.model.Ingredient;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;


public class IngredientDao extends GenericDao {

	/**
	 * Inserts a ingredient.
	 * @param ing The ingredient to persist.
	 * @return the ingredient type.
	 * @throws AlreadyExistsException The ingredient already exists.
	 */
	public Ingredient insert(Ingredient ing) throws AlreadyExistsException {
		EntityManager entityManager = getEntityManager();

		Ingredient existing = findById(ing.getId());
		if (existing != null) {
			throw new AlreadyExistsException("The ingredient named " + ing.getLabel() + " already exists.");
		}
		DatabaseHelper.beginTransaction(entityManager);
		entityManager.persist(ing);
		DatabaseHelper.commitTransactionAndClose(entityManager);
		return ing;
	}

	/**
	 * Finds all ingredient.
	 * @return A list containing all the ingredient.
	 * @throws SQLException 
	 */
	public List<Ingredient> findAll() {
		return getEntityManager().createQuery("from Ingredient", Ingredient.class).getResultList();
	}

	/**
	 * Finds a ingredient by its id.
	 * @return The matching ingredient, otherwise null.
	 * @throws SQLException 
	 */
	public Ingredient findById(long id) {
		return getEntityManager().find(Ingredient.class, id);
	}

	/**
	 * Finds a ingredient by its name.
	 * @return The matching ingredient, otherwise null.
	 * @throws SQLException 
	 */
	public Ingredient findByName(String name) {
		TypedQuery<Ingredient> query = getEntityManager().createQuery("from Ingredient where label = :name", Ingredient.class);
		query.setParameter("name", name);
		return query.getSingleResult();		
	}
}
