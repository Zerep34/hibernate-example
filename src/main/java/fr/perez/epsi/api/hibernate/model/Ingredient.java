package fr.perez.epsi.api.hibernate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ingredient")
public class Ingredient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_ing;
	
	private String label;

	public Ingredient(){}

	public Ingredient(String label) {
		this.label = label;
	}

	public long getId() {
		return id_ing;
	}
	public void setId(long id) {
		this.id_ing = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return "Ingredient [label=" + label + "]";
	}
}
