package fr.perez.epsi.api.hibernate;

import fr.perez.epsi.api.hibernate.dao.IngredientDao;
import fr.perez.epsi.api.hibernate.dao.RecetteDao;
import fr.perez.epsi.api.hibernate.exception.AlreadyExistsException;
import fr.perez.epsi.api.hibernate.model.Ingredient;
import fr.perez.epsi.api.hibernate.model.Recette;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
	
	private static final RecetteDao recetteDao = new RecetteDao();
	private static final IngredientDao ingredientDao = new IngredientDao();
	private static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) throws AlreadyExistsException {
		
		// Appeler le DAO pour récupérer la liste de toutes les recettes présentes en base de données
		logger.debug("Lancement de l'application");
		logger.info("Liste des Recettes présentes en base de données :");
		recetteDao.findAll().forEach(p -> logger.info(p));

		logger.debug("Insertion des Recettes :");

		List<Recette> newRecettes = new ArrayList<>();
		Ingredient ing1 = ingredientDao.findByName("tomates");
		Recette sablaireau = new Recette("Pate bologanaise", "Faire des cuires, mettre de la tomate", Collections.singletonList(ing1));
		sablaireau.setIngredients(Collections.singletonList(ing1));
		newRecettes.add(sablaireau);

		newRecettes.forEach(p -> {
			try {
				logger.info("Recette inserted : " + recetteDao.insert(p));
			} catch (AlreadyExistsException e) {
				logger.error(e.getMessage());
			}
		});
	}

}
