package fr.perez.epsi.api.hibernate.dao;

import fr.perez.epsi.api.hibernate.dao.helper.DatabaseHelper;
import fr.perez.epsi.api.hibernate.exception.AlreadyExistsException;
import fr.perez.epsi.api.hibernate.model.Ingredient;
import fr.perez.epsi.api.hibernate.model.Recette;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class RecetteDao extends GenericDao {

	/**
	 * Inserts a Recette.
	 * @param recette The recette to persist.
	 * @return The persisted recette.
	 * @throws AlreadyExistsException The recette already exists.
	 */
	public Recette insert(Recette recette) throws AlreadyExistsException {
		EntityManager entityManager = getEntityManager();
		
		// Check if number already exists
		Recette existing = findById(recette.getId());
		if (existing != null) {
			throw new AlreadyExistsException("A recette with the number " + recette.getId() + " already exists.");
		}
		DatabaseHelper.beginTransaction(entityManager);
		entityManager.persist(recette);
		DatabaseHelper.commitTransactionAndClose(entityManager);
		return recette;
	}

	/**
	 * Finds all recettes.
	 * @return A list containing all the recettes.
	 */
	public List<Recette> findAll() {
		return getEntityManager().createQuery("select p from Recette p", Recette.class).getResultList();
	}

	/**
	 * Finds a Recette by its id.
	 * @return The matching Recette, otherwise null.
	 * @throws SQLException 
	 */
	public Recette findById(long id) {
		return getEntityManager().find(Recette.class, id);
	}

}
